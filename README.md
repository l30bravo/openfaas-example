# Random Function

Cloud function
it's a returns a random number

# First steps
* [Kimun Openfaas](https://gitlab.com/l30bravo/kimun-openfaas) - Kimun it's from the Mapudungun language that means Knowledge

# How this project it's was created?
1. Create a function from a template, you Use `faas-cli new` to create a template in one of the supported languages, or `faas-cli new --lang Dockerfile` to use a custom Dockerfile, so in this case:
```
faas-cli new --lang go <function-name>
```
* in this case
```
faas-cli new --lang go gohash
```

2. Now have a folder called gohash with a  `handler.go` file inside we are going to add all logic to process one particular thing that we need

3. build the image
```
faas-cli build -f gohash.yml
```

4. deploy the function
```
faas-cli deploy -f gohash.yml
```


