package function

import (
	"encoding/json"
	"log"
)

type Output struct {
	Name  string `json:"type"`
	Value string `json:"value"`
}

// Handle a serverless request
func Handle(req []byte) string {
	isNumber, numValue := isAnumber(string(req))
	var output Output
	if isNumber == true {
		log.Println("creating random string...")
		output = Output{Name: "random-string", Value: randomString(numValue)}
	} else {
		output = Output{Name: "random-string", Value: "-"}
	}
	json_data, err := json.Marshal(output)
	if err != nil {
		log.Println(err.Error())
	}
	log.Println(json_data)
	return string(json_data)
}
