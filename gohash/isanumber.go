package function

import (
	"log"
	"strconv"
)

func isAnumber(input string) (bool, int) {
	val, err := strconv.Atoi(input)
	if err != nil {
		log.Printf("Supplied value %s is not a number\n", input)
		return false, 0
	} else {
		log.Printf("Supplied value %s is a number with value %d\n", input, val)
		return true, val
	}
}
